﻿using System;
namespace Task
{
    public class PlayScreen
    {
        public OrthographicCamera gameCam;
        public TiledMap map;
        public World world;
        public MovementImpl player;
        public static float FREQUENCY = (((float)(1)) / 60);
        public static const  int VEL_ITERATION = 1;
        public static const int POS_ITERATION = 1;


        public PlayScreen()
        {
            gameCam = new OrthographicCamera();
            map = new TiledMap();
            world = new World();
            player = new MovementImpl();
        }
        public OrthographicCamera GetCam() => this.gameCam;
        public TiledMap GetMap() => this.map;
        public World GetWorld() => this.world;
        public MovementImpl GetPlayer() => this.player;


    }
}


