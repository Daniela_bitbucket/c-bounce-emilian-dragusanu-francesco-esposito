﻿using System;

namespace Task
{
    public abstract class Animations : Sprite
    {

        protected TextureAtlas atlas = new TextureAtlas("sprites/Animation_light_Ball.atlas");

        protected Body body;

        protected TextureRegion ballStand;

        protected Array<TextureRegion> frames;

        protected float stateTimer;

        private static const int BOUNDS_WIDTH = 90;

        private static const int BOUNDS_HEIGHT = 90;

        private static const int WIDTH = 120;

        private static const int HEIGHT = 120;

        private static const int PIXEL = 120;

        public Animations()
        {
        }

        public Animations(String nameRegion)
        {
            setRegion(new TextureRegion(this.atlas.findRegion(nameRegion), 0, 0, WIDTH, HEIGHT));
            setBounds(0, 0, BOUNDS_WIDTH, BOUNDS_HEIGHT);
        }

        public Animation SetAnimation(int startFrame, int endFrame, String animationName, int height, int width)
        {
            float duration = 0.1;
            this.frames = new Array();
            for (int i = startFrame; (i < endFrame); i++)
            {
                this.frames.add(new TextureRegion(this.atlas.findRegion(animationName), (i * PIXEL), 0, height, width));
            }

            return new Animation(duration, this.frames);
        }

        public void CleanFrames()
        {
            this.frames.clear();
        }

        public void Dispose()
        {
            this.atlas.dispose();
        }

        public void Draw(Batch batch)
        {
            base.draw(batch);
        }

        protected abstract void Update(float dt);
    }
}
