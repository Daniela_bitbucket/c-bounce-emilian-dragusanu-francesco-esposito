﻿using System;
namespace Task
{

    public interface BallView
    {

        void SetBall(Ball ball);

        float GetStateTimer();

        void Update(float dt);

        void Draw(Batch batch);
    }
}
