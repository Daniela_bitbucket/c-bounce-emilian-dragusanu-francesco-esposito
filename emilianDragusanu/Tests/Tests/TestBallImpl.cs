﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Classes;

namespace Tests
{
    [TestClass]
    public class TestBallImpl
    {

        readonly BallImpl ball;
        readonly float x = 10;
        readonly float y = 10;
        readonly PlayScreen ps = new PlayScreen();
        readonly Bounce game = new Bounce();
        ball = new BallImpl(ps, x, y, game);

        [TestMethod]
        public void TestGetter()
        {
            Assert.IsNotNull(ball);
            Assert.IsNotNull(ball.getBody());
            Assert.IsNotNull(ball.getPosition());
            Assert.IsNotNull(ball.getState());
            Assert.IsNotNull(ball.getHud());
            Assert.IsNotNull(ball.getWorld());
        }

        [TestMethod]
        public void TestSetter()
        {
            Assert.IsFalse(ball.IsJumping());
            ball.SetJumping(true);
            Assert.IsTrue(ball.IsJumping());
            Assert.IsFalse(ball.IsBig());
            ball.SetBig();
            Assert.IsTrue(ball.IsBig());
            Assert.IsFalse(ball.IsSmall());
            ball.SetSmall();
            Assert.IsTrue(ball.IsSmall());
        }
    }
}
