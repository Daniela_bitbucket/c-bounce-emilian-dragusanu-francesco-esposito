﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Classes;

namespace Tests
{
    [TestClass]
    public class TestTypeDefImpl
    {
        readonly float x = 10;
        readonly float y = 10;
        readonly PlayScreen ps = new PlayScreen();
        readonly MapObject rObject = new MapObject();
        readonly TypeDefImpl tDef = new TypeDefImpl(ps, x, y, rObject);

        [TestMethod]
        public void TestMethod1()
        {
            Assert.IsNotNull(tDef);
            Assert.IsNotNull(tDef.GetBody());
            Assert.IsNotNull(tDef.GetFixture());
            Assert.IsNotNull(tDef.GetObject());
            Assert.IsNotNull(tDef.GetPlacement());
            Assert.IsNotNull(tDef.GetPlayScreen());
            Assert.IsNotNull(tDef.GetTaken());
            Assert.IsNotNull(tDef.GetWorld());
        }
    }
}