﻿using System;

namespace Classes
{
    public interface ITypeDef
    {

        Body getBody();

        void setCategoryFilter(int filterId);

        Fixture getFixture();
    }
}